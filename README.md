# Bloodwest Helper

Liver version found at: https://rhiyo.gitlab.io/bloodwest-helper/

A webapp for helping for helping with Bloodwest achievement tracking.

Mostly developed in JavaScript.

Potential line in save where notes are can be found with:
B

First line after:
�
Second line after:
-

Not sure if line that contains the area aswell a few lines above. I believe one is for if the codex is read and the other as if you've found it (it's gone in your inventory.) I assume the achievement requires the latter.

## Running yourself

This is mostly kept on a public repository to make use of pages. However, if you do want to run this yourself you'll have to have npm and run
```
npm install
```
to install the required javascript packages. Then you can just open index.html and use it locally, however you may have issues with [CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS).

## TODO
- Work out the format of the save file (Unity Gameobject Binary Serialized?)
- Work out a way to unserialize