let dropArea = document.getElementById('drop-area');

const CREATURES_NEEDED = 45

dropArea.addEventListener('dragenter', highlight, false)
dropArea.addEventListener('dragleave', unhighlight, false)
dropArea.addEventListener('dragover', highlight, false)
dropArea.addEventListener('drop', unhighlight, false)

;['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
    dropArea.addEventListener(eventName, preventDefaults, false)
})

function preventDefaults (e) {
    e.preventDefault()
    e.stopPropagation()
}

function highlight(e) {
    dropArea.classList.add('highlight')
}

function unhighlight(e) {
    dropArea.classList.remove('highlight')
}

dropArea.addEventListener('drop', handleDrop, false)

function handleDrop(e) {
    let dt = e.dataTransfer
    let files = dt.files

    if(files[0].name.endsWith('.blood_west_save')){
        manageSaveFile(files[0])
    }
}

function displayResults(save_data) {
    var showText = ""
    for (var area in save_data.Areas){
        showText += "<p id='chapter'><strong>" + save_data.Areas[area] + "</strong></p>"
        showText += "<table id='results'><thead>";
        showText += "<th>Note Name</th>";
        showText += "<th>Found</th>";
        showText += "</thead><tbody>";
        for (var key in save_data.Creatures){
            if (save_data.Creatures[key].chapter == area){
                showText += "<tr class='" + save_data.Creatures[key].found + "'>";
                showText +=  "<td class='" + save_data.Creatures[key].found + "'>" + save_data.Creatures[key].name + '</td>';
                showText +=  "<td class='" + save_data.Creatures[key].found + "'>" + save_data.Creatures[key].found + '</td>';
                showText += '</tr>'
            }
    
        }
        showText += "</tbody></table><br>";
    }
    document.getElementById('result').scrollIntoView()
    document.getElementById('result').innerHTML = showText;
}

function manageSaveFile(file){
    document.getElementById('result').innerHTML = "Reading file...";
    var file_loc = "BWData.yaml"
    var client = new XMLHttpRequest();
    var username = file.name.split('.').slice(0, -1).join('.')
    client.open('GET',file_loc);
    client.onloadend =  function() {
        var save_data = jsyaml.load(client.responseText);
        for (var key in save_data.Creatures){
         
            save_data.Creatures[key]['found'] = "NOT FOUND"             
         }
         var reader = new FileReader();

         reader.onload = function(e) {
                 // Print the contents of the file
                 var text = e.target.result;

                 var lines = text.split(/[\r\n]+/g); // tolerate both Windows and Unix linebreaks

                 var found_line = false;
                 for(var i = 0; i < lines.length; i++) { 
                    if (lines[i].includes(username)){
                        
                        for (var key in save_data.Creatures){
                            if(lines[i].includes(key)){
                                save_data.Creatures[key]['found'] = "FOUND"
                                found_line = true
                            }          
                         }
                         if (found_line == true){
                            break;
                        }
                    }
                 }
                 displayResults(save_data)
         };

         reader.readAsBinaryString(file);
    };
    
    client.send();
};

